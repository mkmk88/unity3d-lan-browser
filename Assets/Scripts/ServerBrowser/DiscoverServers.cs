﻿using UnityEngine;
using System;
using System.Collections;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;

public class DiscoverServers : MonoBehaviour
{
    private UdpClient receiver;
    private bool readyToProcess = false;
    private ArrayList servers;

    public int ListeningPort = 7767;

    public void Awake()
    {
        servers = new ArrayList();
    }

    private void receiveServerData(IAsyncResult result)
    {
        byte[] data = null;
        ServerData serverData;
        IPEndPoint ep = new IPEndPoint(IPAddress.Any, ListeningPort);        

        data = receiver.EndReceive(result, ref ep);

        if (data != null)
        {
            serverData = new ServerData();
        }
        else
        {
            readyToProcess = true;
            return;
        }

        serverData.ParseServerData(data);
        // todo: assert
        foreach (ServerData server in servers)
        {
            if (server.IP == serverData.IP)
            {           
                readyToProcess = true;
                return; // we don't want to add the same server
            }
        }

        servers.Add(serverData);
        readyToProcess = true;      
    }

    public void Update()
    {
        if (readyToProcess)
        {
            readyToProcess = false;
            receiver.BeginReceive(new AsyncCallback(receiveServerData), null);
        }
    }

    public void StartListening()
    {
        receiver = new UdpClient(ListeningPort);
        readyToProcess = true;
    }

    public void StopListening()
    {
        readyToProcess = false;
        receiver.Close();
    }

    public ArrayList GetServers()
    {
        return servers;
    }
}
