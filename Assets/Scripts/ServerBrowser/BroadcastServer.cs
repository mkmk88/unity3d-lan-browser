﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;
using System.Text;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;

public class BroadcastServer : MonoBehaviour
{
    private UdpClient broadcaster;

    public float Rate = 1.0f;
    public string ServerName = "UnityServer";
    public int BroadcastPort = 7767;
    public int MaxPlayers = 2;
    public int ServerPort = 7777;
    public int CurrentPlayers = 0;
    
    private IPAddress getBroadcastAddress(IPAddress address, IPAddress subnetMask)
    {
        byte[] rawIP = address.GetAddressBytes();
        byte[] rawSubnetMask = subnetMask.GetAddressBytes();
        byte[] broadcastAddress = new byte[rawIP.Length];

        for (int i = 0; i < broadcastAddress.Length; i++)
        {
            broadcastAddress[i] = (byte)(rawIP[i] | (rawSubnetMask[i] ^ 255));
        }

        return new IPAddress(broadcastAddress);
    }

    private IPAddress getSubnetMask(IPAddress address)
    {
        foreach (NetworkInterface adapter in NetworkInterface.GetAllNetworkInterfaces())
        {
            foreach (UnicastIPAddressInformation unicastIPAddressInformation in adapter.GetIPProperties().UnicastAddresses)
            {
                if (unicastIPAddressInformation.Address.AddressFamily == AddressFamily.InterNetwork)
                {
                    if (address.Equals(unicastIPAddressInformation.Address))
                    {
                        return unicastIPAddressInformation.IPv4Mask;
                    }
                }
            }
        }

        return null;
    }

    private void broadcastServerData()
    {
        string msg = ServerData.createServerData(ServerName, Network.player.ipAddress, ServerPort,
            CurrentPlayers, MaxPlayers);

        broadcaster.Send(Encoding.ASCII.GetBytes(msg), msg.Length);
    }

    public void StartBroadcast()
    {
        IPAddress serverIP = IPAddress.Parse(Network.player.ipAddress);
        IPAddress subnetMask = getSubnetMask(serverIP);
        IPAddress broadcast = getBroadcastAddress(serverIP, subnetMask);

        broadcaster = new UdpClient(BroadcastPort - 1, AddressFamily.InterNetwork);
        IPEndPoint ep = new IPEndPoint(broadcast, BroadcastPort);
        broadcaster.Connect(ep);
        InvokeRepeating("broadcastServerData", 0, Rate);

    }

    public void StopBroadcast()
    {
        CancelInvoke("broadcastServerData");
        broadcaster.Close();
    }

}
