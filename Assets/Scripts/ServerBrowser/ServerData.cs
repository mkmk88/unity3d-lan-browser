﻿using UnityEngine;
using System;
using System.Text;
using System.Collections;

public class ServerData
{
    private string m_name = string.Empty;

    public string Name
    {
        get
        {
            return m_name;
        }
        set
        {
            m_name = value;
        }
    }

    private string m_ip = string.Empty;

    public string IP
    {
        get
        {
            return m_ip;
        }
        set
        {
            m_ip = value;
        }
    }

    private int m_port = 0;

    public int Port
    {
        get
        {
            return m_port;
        }
        set
        {
            m_port = value;
        }
    }

    private int m_currentPlayers = 0;

    public int CurrentPlayers
    {
        get
        {
            return m_currentPlayers;
        }
        set
        {
            m_currentPlayers = value;
        }
    }

    private int m_maxPlayers = 0;

    public int MaxPlayers
    {
        get
        {
            return m_maxPlayers;
        }
        set
        {
            m_maxPlayers = value;
        }
    }

    private int m_fields = 5;

    static public string createServerData(string name, string ip, int port, int currentPlayers, int maxPlayers)
    {
        return name + ":" + ip + ":" + port.ToString() + ":" + currentPlayers.ToString() + ":" + maxPlayers.ToString();
    }

    public void ParseServerData(byte[] data)
    {
        string[] extractedData;
        char[] splitChar = { ':' };
        string serverMsg = Encoding.ASCII.GetString(data);

        extractedData = serverMsg.Split(splitChar, m_fields);

        m_name = extractedData[0];
        m_ip = extractedData[1];
        m_port = Int32.Parse(extractedData[2]);
        m_currentPlayers = Int32.Parse(extractedData[3]);
        m_maxPlayers = Int32.Parse(extractedData[4]);

    }
}
