﻿using UnityEngine;
using System.Collections;

public class DummyServersDisplayer : MonoBehaviour
{
    private bool isWaiting;
    private float delayTime = 1.0f;
    private DiscoverServers discoverServers;
    private ArrayList servers;
    IEnumerator delay()
    {
        isWaiting = true;
        yield return new WaitForSeconds(delayTime);
        isWaiting = false;
    }

    void Start()
    {
        discoverServers = GetComponent<DiscoverServers>();
        servers = discoverServers.GetServers();
    }

	// Update is called once per frame
	void Update ()
    {
        if (isWaiting)
        {
            return;
        }       

        if (servers == null)
        {
            return;
        }

        foreach (ServerData server in servers)
        {
            Debug.Log(server.Name + " " + server.IP + " " + server.Port.ToString() + " "
                + server.CurrentPlayers.ToString() + " " + server.CurrentPlayers.ToString()
                + " " + server.MaxPlayers.ToString());
        }
	
        StartCoroutine(delay());
	}
}
